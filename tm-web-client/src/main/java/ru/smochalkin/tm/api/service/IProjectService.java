package ru.smochalkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    List<Project> findAll(@NotNull String userId);

    void addAll(Collection<Project> collection);

    void addAll(String userId, @Nullable Collection<Project> collection);

    Project save(Project entity);

    Project save(String userId, @Nullable Project entity);

    void create();

    void create(String userId);

    Project findById(String id);

    Project findById(@NotNull String userId, @Nullable String id);

    void clear();

    void clear(@NotNull String userId);

    void removeById(String id);

    void removeById(@NotNull String userId, @Nullable String id);

    void remove(Project entity);

}
