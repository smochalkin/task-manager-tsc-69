package ru.smochalkin.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.smochalkin.tm.api.endpoint.ITaskEndpoint;
import ru.smochalkin.tm.api.service.ITaskService;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService
@RestController
@RequestMapping("/api/tasks")
public class TaskEndpoint implements ITaskEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return new ArrayList<>(taskService.findAll(UserUtil.getUserId()));
    }

    @Override
    @WebMethod
    @GetMapping("/find/{id}")
    public Task find(@PathVariable("id") @WebParam(name = "id") final String id) {
        return taskService.findById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/create")
    public Task create(@RequestBody @WebParam(name = "task") final Task task) {
        taskService.save(UserUtil.getUserId(), task);
        return task;
    }

    @Override
    @WebMethod
    @PostMapping("/createAll")
    public List<Task> createAll(@RequestBody @WebParam(name = "tasks") final List<Task> tasks) {
        taskService.addAll(UserUtil.getUserId(), tasks);
        return tasks;
    }

    @Override
    @WebMethod
    @PutMapping("/save")
    public Task save(@RequestBody @WebParam(name = "task") final Task task) {
        taskService.save(UserUtil.getUserId(), task);
        return task;
    }

    @Override
    @WebMethod
    @PutMapping("/saveAll")
    public List<Task> saveAll(@RequestBody @WebParam(name = "tasks") final List<Task> tasks) {
        taskService.addAll(UserUtil.getUserId(), tasks);
        return tasks;
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") @WebParam(name = "id") final String id) {
        taskService.removeById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        taskService.clear(UserUtil.getUserId());
    }

}
