package ru.smochalkin.tm.client;

import feign.Feign;
import okhttp3.JavaNetCookieJar;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.smochalkin.tm.api.endpoint.ITaskEndpoint;
import ru.smochalkin.tm.model.Task;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.List;

public interface TaskFeignClient extends ITaskEndpoint {

    String URL = "http://localhost:8080/api/tasks";

    static TaskFeignClient client() {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        final CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        final okhttp3.OkHttpClient.Builder builder = new okhttp3.OkHttpClient().newBuilder();
        builder.cookieJar(new JavaNetCookieJar(cookieManager));
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskFeignClient.class, URL);
    }

    @GetMapping("/findAll")
    List<Task> findAll();

    @GetMapping("/find/{id}")
    Task find(@PathVariable("id") final String id);

    @PostMapping("/create")
    Task create(@RequestBody final Task task);

    @PostMapping("/createAll")
    List<Task> createAll(@RequestBody final List<Task> tasks);

    @PutMapping("/save")
    Task save(@RequestBody final Task task);

    @PutMapping("/saveAll")
    List<Task> saveAll(@RequestBody final List<Task> tasks);

    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable("id") final String id);

    @DeleteMapping("/deleteAll")
    void deleteAll();

}
