package ru.smochalkin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.smochalkin.tm.api.service.IProjectService;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.CustomUser;
import ru.smochalkin.tm.model.Project;

@Controller
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("/project/create")
    public String create(
            @AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user
    ) {
        projectService.create(user.getUserId());
        return "redirect:/projects";
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("/project/delete/{id}")
    public String delete(
            @AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user,
            @PathVariable("id") String id
    ) {
        projectService.removeById(user.getUserId(), id);
        return "redirect:/projects";
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user,
            @PathVariable("id") String id, Model model
    ) {
        return new ModelAndView(
                "project-edit",
                "project", projectService.findById(user.getUserId(), id)
        );
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @PostMapping("/project/edit")
    public String edit(
            @AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user,
            @ModelAttribute("project") Project project
    ) {
        projectService.save(user.getUserId(), project);
        return "redirect:/projects";
    }

    @ModelAttribute("viewName")
    public String getViewName() {
        return "Project edit";
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

}