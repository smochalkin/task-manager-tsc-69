package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.smochalkin.tm.api.service.*;
import ru.smochalkin.tm.configuration.ContextConfiguration;
import ru.smochalkin.tm.dto.TaskDto;
import ru.smochalkin.tm.enumerated.Status;

import java.util.List;
import java.util.UUID;

public class TaskServiceTest {

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private IUserService userService;

    @NotNull
    private IProjectTaskService projectTaskService;

    private static String USER_ID_1;

    private static String USER_ID_2;

    private static String PROJECT_ID = UUID.randomUUID().toString();

    private int taskCount;

    @Before
    public void init() {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ContextConfiguration.class);
        @NotNull final IPropertyService propertyService = new PropertyService();
        userService = context.getBean(IUserService.class);
        userService.create("test1", "1", "@");
        userService.create("test2", "2", "@");
        USER_ID_1 = userService.findByLogin("test1").getId();
        USER_ID_2 = userService.findByLogin("test2").getId();
        projectService = context.getBean(IProjectService.class);
        projectService.create(USER_ID_1, "test", "test");
        PROJECT_ID = projectService.findByName(USER_ID_1, "test").getId();
        projectTaskService = context.getBean(IProjectTaskService.class);
        taskService = context.getBean(ITaskService.class);
        taskService.create(USER_ID_1, "test1", "test1");
        taskService.create(USER_ID_2, "test2", "test2");
        taskCount = taskService.getCount();
    }

    @After
    public void end() {
        taskService.removeByName(USER_ID_1,"test1");
        taskService.removeByName(USER_ID_2,"test2");
        taskService.removeByName(USER_ID_1,"test3");
        taskService.removeByName(USER_ID_1,"new name");
        projectService.removeByName(USER_ID_1,"test");
        userService.removeByLogin("test1");
        userService.removeByLogin("test2");
    }

    @Test
    public void createTest() {
        taskService.create(USER_ID_1, "test3", "test3");
        Assert.assertEquals(taskCount + 1, taskService.getCount());
    }


    @Test
    public void findAllTest() {
        @NotNull List<TaskDto> taskDtoList = taskService.findAll(USER_ID_1);
        Assert.assertEquals(1, taskDtoList.size());
    }

    @Test
    public void findByIdTest() {
        @NotNull final String taskId = taskService.findByName(USER_ID_1,"test1").getId();
        Assert.assertEquals(taskId, taskService.findById(USER_ID_1, taskId).getId());
    }

    @Test
    public void findByNameTest() {
        Assert.assertEquals("test1", taskService.findByName(USER_ID_1,"test1").getName());
    }

    @Test
    public void findByIndexTest() {
        Assert.assertEquals("test1", taskService.findByIndex(USER_ID_1,0).getName());
    }

    @Test
    public void removeByIdTest() {
        @NotNull final String taskId = taskService.findByName(USER_ID_1,"test1").getId();
        taskService.removeById(USER_ID_1, taskId);
        Assert.assertNull(taskService.findById(USER_ID_1, taskId));
    }

    @Test
    public void removeByNameTest() {
        taskService.removeByName(USER_ID_1, "test1");
        Assert.assertNull(taskService.findByName(USER_ID_1, "test1"));
    }

    @Test
    public void removeByIndexTest() {
        taskService.removeByIndex(USER_ID_1, 0);
        Assert.assertNull(taskService.findByIndex(USER_ID_1, 0));
    }

    @Test
    public void updateByIdTest() {
        @NotNull final String taskId = taskService.findByName(USER_ID_1,"test1").getId();
        taskService.updateById(USER_ID_1, taskId, "new name", "new desc");
        @NotNull final TaskDto taskDto = taskService.findById(USER_ID_1, taskId);
        Assert.assertEquals("new name", taskDto.getName());
        Assert.assertEquals("new desc", taskDto.getDescription());
    }

    @Test
    public void updateByIndexTest() {
        taskService.updateByIndex(USER_ID_1, 0, "new name", "new desc");
        @NotNull final TaskDto taskDto = taskService.findByIndex(USER_ID_1, 0);
        Assert.assertEquals("new name", taskDto.getName());
        Assert.assertEquals("new desc", taskDto.getDescription());
    }

    @Test
    public void updateStatusByIdTest() {
        @NotNull final String taskId = taskService.findByName(USER_ID_1,"test1").getId();
        taskService.updateStatusById(USER_ID_1, taskId, "COMPLETED");
        @NotNull final TaskDto taskDto = taskService.findById(USER_ID_1, taskId);
        Assert.assertEquals(Status.COMPLETED, taskDto.getStatus());
    }

    @Test
    public void updateStatusByNameTest() {
        taskService.updateStatusByName(USER_ID_1, "test1", "COMPLETED");
        @NotNull final TaskDto taskDto = taskService.findByName(USER_ID_1, "test1");
        Assert.assertEquals(Status.COMPLETED, taskDto.getStatus());
    }

    @Test
    public void updateStatusByIndexTest() {
        taskService.updateStatusByIndex(USER_ID_1, 0, "COMPLETED");
        @NotNull final TaskDto taskDto = taskService.findByIndex(USER_ID_1, 0);
        Assert.assertEquals(Status.COMPLETED, taskDto.getStatus());
    }

    @Test
    public void isNotIndex() {
        Assert.assertTrue(taskService.isNotIndex(USER_ID_1, 99));
        Assert.assertFalse(taskService.isNotIndex(USER_ID_1, 0));
    }

    @Test
    public void bindTaskByProjectIdTest() {
        @NotNull final String taskId = taskService.findByName(USER_ID_1,"test1").getId();
        projectTaskService.bindTaskByProjectId(USER_ID_1, PROJECT_ID, taskId);
        @NotNull final TaskDto taskDto = taskService.findById(USER_ID_1, taskId);
        Assert.assertEquals(PROJECT_ID, taskDto.getProjectId());
    }

    @Test
    public void unbindTaskByProjectIdTest() {
        @NotNull final String taskId = taskService.findByName(USER_ID_1,"test1").getId();
        projectTaskService.bindTaskByProjectId(USER_ID_1, PROJECT_ID, taskId);
        Assert.assertEquals(PROJECT_ID, taskService.findById(USER_ID_1, taskId).getProjectId());
        projectTaskService.unbindTaskByProjectId(USER_ID_1, PROJECT_ID, taskId);
        Assert.assertNotEquals(PROJECT_ID, taskService.findById(USER_ID_1, taskId).getProjectId());

    }

    @Test
    public void findTasksByProjectIdTest() {
        @NotNull final String taskId = taskService.findByName(USER_ID_1,"test1").getId();
        projectTaskService.bindTaskByProjectId(USER_ID_1, PROJECT_ID, taskId);
        Assert.assertEquals(1, projectTaskService.findTasksByProjectId(USER_ID_1, PROJECT_ID).size());
    }

    @Test
    public void removeProjectByIdTest() {
        @NotNull final String taskId = taskService.findByName(USER_ID_1,"test1").getId();
        projectTaskService.bindTaskByProjectId(USER_ID_1, PROJECT_ID, taskId);
        Assert.assertEquals(1, projectTaskService.findTasksByProjectId(USER_ID_1, PROJECT_ID).size());
        projectTaskService.removeProjectById(PROJECT_ID);
        Assert.assertEquals(0, projectTaskService.findTasksByProjectId(USER_ID_1, PROJECT_ID).size());
    }

}
