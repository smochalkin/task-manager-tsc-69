package ru.smochalkin.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.dto.UserDto;
import ru.smochalkin.tm.dto.result.Result;
import ru.smochalkin.tm.dto.SessionDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @Nullable
    @WebMethod
    @SneakyThrows
    UserDto findUserBySession(
            @WebParam(name = "session", partName = "session") @NotNull SessionDto sessionDto
    );

    @WebMethod
    @SneakyThrows
    Result userSetPassword(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "password") @Nullable String password
    );

    @WebMethod
    @SneakyThrows
    Result userUpdate(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "firstName") @Nullable String firstName,
            @WebParam(name = "lastName") @Nullable String lastName,
            @WebParam(name = "middleName") @Nullable String middleName
    );

}
