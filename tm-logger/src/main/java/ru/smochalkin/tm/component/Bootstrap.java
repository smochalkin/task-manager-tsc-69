package ru.smochalkin.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.api.service.IReceiverService;

import javax.jms.MessageListener;

@Component
public class Bootstrap {

    @NotNull
    @Autowired
    IReceiverService receiverService;

    @NotNull
    @Autowired
    MessageListener logListener;

    @SneakyThrows
    public void init() {
        receiverService.receive(logListener);
    }

}
