
package ru.smochalkin.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.smochalkin.tm.endpoint package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _BindTaskByProjectId_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "bindTaskByProjectId");
    private final static QName _BindTaskByProjectIdResponse_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "bindTaskByProjectIdResponse");
    private final static QName _ChangeTaskStatusById_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "changeTaskStatusById");
    private final static QName _ChangeTaskStatusByIdResponse_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "changeTaskStatusByIdResponse");
    private final static QName _ChangeTaskStatusByIndex_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "changeTaskStatusByIndex");
    private final static QName _ChangeTaskStatusByIndexResponse_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "changeTaskStatusByIndexResponse");
    private final static QName _ChangeTaskStatusByName_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "changeTaskStatusByName");
    private final static QName _ChangeTaskStatusByNameResponse_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "changeTaskStatusByNameResponse");
    private final static QName _ClearTasks_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "clearTasks");
    private final static QName _ClearTasksResponse_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "clearTasksResponse");
    private final static QName _CreateTask_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "createTask");
    private final static QName _CreateTaskResponse_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "createTaskResponse");
    private final static QName _FindTaskAll_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "findTaskAll");
    private final static QName _FindTaskAllResponse_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "findTaskAllResponse");
    private final static QName _FindTaskAllSorted_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "findTaskAllSorted");
    private final static QName _FindTaskAllSortedResponse_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "findTaskAllSortedResponse");
    private final static QName _FindTaskById_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "findTaskById");
    private final static QName _FindTaskByIdResponse_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "findTaskByIdResponse");
    private final static QName _FindTaskByIndex_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "findTaskByIndex");
    private final static QName _FindTaskByIndexResponse_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "findTaskByIndexResponse");
    private final static QName _FindTaskByName_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "findTaskByName");
    private final static QName _FindTaskByNameResponse_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "findTaskByNameResponse");
    private final static QName _FindTasksByProjectId_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "findTasksByProjectId");
    private final static QName _FindTasksByProjectIdResponse_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "findTasksByProjectIdResponse");
    private final static QName _RemoveTaskById_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "removeTaskById");
    private final static QName _RemoveTaskByIdResponse_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "removeTaskByIdResponse");
    private final static QName _RemoveTaskByIndex_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "removeTaskByIndex");
    private final static QName _RemoveTaskByIndexResponse_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "removeTaskByIndexResponse");
    private final static QName _RemoveTaskByName_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "removeTaskByName");
    private final static QName _RemoveTaskByNameResponse_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "removeTaskByNameResponse");
    private final static QName _UnbindTaskByProjectId_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "unbindTaskByProjectId");
    private final static QName _UnbindTaskByProjectIdResponse_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "unbindTaskByProjectIdResponse");
    private final static QName _UpdateTaskById_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "updateTaskById");
    private final static QName _UpdateTaskByIdResponse_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "updateTaskByIdResponse");
    private final static QName _UpdateTaskByIndex_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "updateTaskByIndex");
    private final static QName _UpdateTaskByIndexResponse_QNAME = new QName("http://endpoint.tm.smochalkin.ru/", "updateTaskByIndexResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.smochalkin.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BindTaskByProjectId }
     * 
     */
    public BindTaskByProjectId createBindTaskByProjectId() {
        return new BindTaskByProjectId();
    }

    /**
     * Create an instance of {@link BindTaskByProjectIdResponse }
     * 
     */
    public BindTaskByProjectIdResponse createBindTaskByProjectIdResponse() {
        return new BindTaskByProjectIdResponse();
    }

    /**
     * Create an instance of {@link ChangeTaskStatusById }
     * 
     */
    public ChangeTaskStatusById createChangeTaskStatusById() {
        return new ChangeTaskStatusById();
    }

    /**
     * Create an instance of {@link ChangeTaskStatusByIdResponse }
     * 
     */
    public ChangeTaskStatusByIdResponse createChangeTaskStatusByIdResponse() {
        return new ChangeTaskStatusByIdResponse();
    }

    /**
     * Create an instance of {@link ChangeTaskStatusByIndex }
     * 
     */
    public ChangeTaskStatusByIndex createChangeTaskStatusByIndex() {
        return new ChangeTaskStatusByIndex();
    }

    /**
     * Create an instance of {@link ChangeTaskStatusByIndexResponse }
     * 
     */
    public ChangeTaskStatusByIndexResponse createChangeTaskStatusByIndexResponse() {
        return new ChangeTaskStatusByIndexResponse();
    }

    /**
     * Create an instance of {@link ChangeTaskStatusByName }
     * 
     */
    public ChangeTaskStatusByName createChangeTaskStatusByName() {
        return new ChangeTaskStatusByName();
    }

    /**
     * Create an instance of {@link ChangeTaskStatusByNameResponse }
     * 
     */
    public ChangeTaskStatusByNameResponse createChangeTaskStatusByNameResponse() {
        return new ChangeTaskStatusByNameResponse();
    }

    /**
     * Create an instance of {@link ClearTasks }
     * 
     */
    public ClearTasks createClearTasks() {
        return new ClearTasks();
    }

    /**
     * Create an instance of {@link ClearTasksResponse }
     * 
     */
    public ClearTasksResponse createClearTasksResponse() {
        return new ClearTasksResponse();
    }

    /**
     * Create an instance of {@link CreateTask }
     * 
     */
    public CreateTask createCreateTask() {
        return new CreateTask();
    }

    /**
     * Create an instance of {@link CreateTaskResponse }
     * 
     */
    public CreateTaskResponse createCreateTaskResponse() {
        return new CreateTaskResponse();
    }

    /**
     * Create an instance of {@link FindTaskAll }
     * 
     */
    public FindTaskAll createFindTaskAll() {
        return new FindTaskAll();
    }

    /**
     * Create an instance of {@link FindTaskAllResponse }
     * 
     */
    public FindTaskAllResponse createFindTaskAllResponse() {
        return new FindTaskAllResponse();
    }

    /**
     * Create an instance of {@link FindTaskAllSorted }
     * 
     */
    public FindTaskAllSorted createFindTaskAllSorted() {
        return new FindTaskAllSorted();
    }

    /**
     * Create an instance of {@link FindTaskAllSortedResponse }
     * 
     */
    public FindTaskAllSortedResponse createFindTaskAllSortedResponse() {
        return new FindTaskAllSortedResponse();
    }

    /**
     * Create an instance of {@link FindTaskById }
     * 
     */
    public FindTaskById createFindTaskById() {
        return new FindTaskById();
    }

    /**
     * Create an instance of {@link FindTaskByIdResponse }
     * 
     */
    public FindTaskByIdResponse createFindTaskByIdResponse() {
        return new FindTaskByIdResponse();
    }

    /**
     * Create an instance of {@link FindTaskByIndex }
     * 
     */
    public FindTaskByIndex createFindTaskByIndex() {
        return new FindTaskByIndex();
    }

    /**
     * Create an instance of {@link FindTaskByIndexResponse }
     * 
     */
    public FindTaskByIndexResponse createFindTaskByIndexResponse() {
        return new FindTaskByIndexResponse();
    }

    /**
     * Create an instance of {@link FindTaskByName }
     * 
     */
    public FindTaskByName createFindTaskByName() {
        return new FindTaskByName();
    }

    /**
     * Create an instance of {@link FindTaskByNameResponse }
     * 
     */
    public FindTaskByNameResponse createFindTaskByNameResponse() {
        return new FindTaskByNameResponse();
    }

    /**
     * Create an instance of {@link FindTasksByProjectId }
     * 
     */
    public FindTasksByProjectId createFindTasksByProjectId() {
        return new FindTasksByProjectId();
    }

    /**
     * Create an instance of {@link FindTasksByProjectIdResponse }
     * 
     */
    public FindTasksByProjectIdResponse createFindTasksByProjectIdResponse() {
        return new FindTasksByProjectIdResponse();
    }

    /**
     * Create an instance of {@link RemoveTaskById }
     * 
     */
    public RemoveTaskById createRemoveTaskById() {
        return new RemoveTaskById();
    }

    /**
     * Create an instance of {@link RemoveTaskByIdResponse }
     * 
     */
    public RemoveTaskByIdResponse createRemoveTaskByIdResponse() {
        return new RemoveTaskByIdResponse();
    }

    /**
     * Create an instance of {@link RemoveTaskByIndex }
     * 
     */
    public RemoveTaskByIndex createRemoveTaskByIndex() {
        return new RemoveTaskByIndex();
    }

    /**
     * Create an instance of {@link RemoveTaskByIndexResponse }
     * 
     */
    public RemoveTaskByIndexResponse createRemoveTaskByIndexResponse() {
        return new RemoveTaskByIndexResponse();
    }

    /**
     * Create an instance of {@link RemoveTaskByName }
     * 
     */
    public RemoveTaskByName createRemoveTaskByName() {
        return new RemoveTaskByName();
    }

    /**
     * Create an instance of {@link RemoveTaskByNameResponse }
     * 
     */
    public RemoveTaskByNameResponse createRemoveTaskByNameResponse() {
        return new RemoveTaskByNameResponse();
    }

    /**
     * Create an instance of {@link UnbindTaskByProjectId }
     * 
     */
    public UnbindTaskByProjectId createUnbindTaskByProjectId() {
        return new UnbindTaskByProjectId();
    }

    /**
     * Create an instance of {@link UnbindTaskByProjectIdResponse }
     * 
     */
    public UnbindTaskByProjectIdResponse createUnbindTaskByProjectIdResponse() {
        return new UnbindTaskByProjectIdResponse();
    }

    /**
     * Create an instance of {@link UpdateTaskById }
     * 
     */
    public UpdateTaskById createUpdateTaskById() {
        return new UpdateTaskById();
    }

    /**
     * Create an instance of {@link UpdateTaskByIdResponse }
     * 
     */
    public UpdateTaskByIdResponse createUpdateTaskByIdResponse() {
        return new UpdateTaskByIdResponse();
    }

    /**
     * Create an instance of {@link UpdateTaskByIndex }
     * 
     */
    public UpdateTaskByIndex createUpdateTaskByIndex() {
        return new UpdateTaskByIndex();
    }

    /**
     * Create an instance of {@link UpdateTaskByIndexResponse }
     * 
     */
    public UpdateTaskByIndexResponse createUpdateTaskByIndexResponse() {
        return new UpdateTaskByIndexResponse();
    }

    /**
     * Create an instance of {@link SessionDto }
     * 
     */
    public SessionDto createSessionDto() {
        return new SessionDto();
    }

    /**
     * Create an instance of {@link TaskDto }
     * 
     */
    public TaskDto createTaskDto() {
        return new TaskDto();
    }

    /**
     * Create an instance of {@link AbstractBusinessEntityDto }
     * 
     */
    public AbstractBusinessEntityDto createAbstractBusinessEntityDto() {
        return new AbstractBusinessEntityDto();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BindTaskByProjectId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link BindTaskByProjectId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "bindTaskByProjectId")
    public JAXBElement<BindTaskByProjectId> createBindTaskByProjectId(BindTaskByProjectId value) {
        return new JAXBElement<BindTaskByProjectId>(_BindTaskByProjectId_QNAME, BindTaskByProjectId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BindTaskByProjectIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link BindTaskByProjectIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "bindTaskByProjectIdResponse")
    public JAXBElement<BindTaskByProjectIdResponse> createBindTaskByProjectIdResponse(BindTaskByProjectIdResponse value) {
        return new JAXBElement<BindTaskByProjectIdResponse>(_BindTaskByProjectIdResponse_QNAME, BindTaskByProjectIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeTaskStatusById }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ChangeTaskStatusById }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "changeTaskStatusById")
    public JAXBElement<ChangeTaskStatusById> createChangeTaskStatusById(ChangeTaskStatusById value) {
        return new JAXBElement<ChangeTaskStatusById>(_ChangeTaskStatusById_QNAME, ChangeTaskStatusById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeTaskStatusByIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ChangeTaskStatusByIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "changeTaskStatusByIdResponse")
    public JAXBElement<ChangeTaskStatusByIdResponse> createChangeTaskStatusByIdResponse(ChangeTaskStatusByIdResponse value) {
        return new JAXBElement<ChangeTaskStatusByIdResponse>(_ChangeTaskStatusByIdResponse_QNAME, ChangeTaskStatusByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeTaskStatusByIndex }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ChangeTaskStatusByIndex }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "changeTaskStatusByIndex")
    public JAXBElement<ChangeTaskStatusByIndex> createChangeTaskStatusByIndex(ChangeTaskStatusByIndex value) {
        return new JAXBElement<ChangeTaskStatusByIndex>(_ChangeTaskStatusByIndex_QNAME, ChangeTaskStatusByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeTaskStatusByIndexResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ChangeTaskStatusByIndexResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "changeTaskStatusByIndexResponse")
    public JAXBElement<ChangeTaskStatusByIndexResponse> createChangeTaskStatusByIndexResponse(ChangeTaskStatusByIndexResponse value) {
        return new JAXBElement<ChangeTaskStatusByIndexResponse>(_ChangeTaskStatusByIndexResponse_QNAME, ChangeTaskStatusByIndexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeTaskStatusByName }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ChangeTaskStatusByName }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "changeTaskStatusByName")
    public JAXBElement<ChangeTaskStatusByName> createChangeTaskStatusByName(ChangeTaskStatusByName value) {
        return new JAXBElement<ChangeTaskStatusByName>(_ChangeTaskStatusByName_QNAME, ChangeTaskStatusByName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeTaskStatusByNameResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ChangeTaskStatusByNameResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "changeTaskStatusByNameResponse")
    public JAXBElement<ChangeTaskStatusByNameResponse> createChangeTaskStatusByNameResponse(ChangeTaskStatusByNameResponse value) {
        return new JAXBElement<ChangeTaskStatusByNameResponse>(_ChangeTaskStatusByNameResponse_QNAME, ChangeTaskStatusByNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearTasks }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ClearTasks }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "clearTasks")
    public JAXBElement<ClearTasks> createClearTasks(ClearTasks value) {
        return new JAXBElement<ClearTasks>(_ClearTasks_QNAME, ClearTasks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearTasksResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ClearTasksResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "clearTasksResponse")
    public JAXBElement<ClearTasksResponse> createClearTasksResponse(ClearTasksResponse value) {
        return new JAXBElement<ClearTasksResponse>(_ClearTasksResponse_QNAME, ClearTasksResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateTask }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateTask }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "createTask")
    public JAXBElement<CreateTask> createCreateTask(CreateTask value) {
        return new JAXBElement<CreateTask>(_CreateTask_QNAME, CreateTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateTaskResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateTaskResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "createTaskResponse")
    public JAXBElement<CreateTaskResponse> createCreateTaskResponse(CreateTaskResponse value) {
        return new JAXBElement<CreateTaskResponse>(_CreateTaskResponse_QNAME, CreateTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskAll }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindTaskAll }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "findTaskAll")
    public JAXBElement<FindTaskAll> createFindTaskAll(FindTaskAll value) {
        return new JAXBElement<FindTaskAll>(_FindTaskAll_QNAME, FindTaskAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskAllResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindTaskAllResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "findTaskAllResponse")
    public JAXBElement<FindTaskAllResponse> createFindTaskAllResponse(FindTaskAllResponse value) {
        return new JAXBElement<FindTaskAllResponse>(_FindTaskAllResponse_QNAME, FindTaskAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskAllSorted }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindTaskAllSorted }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "findTaskAllSorted")
    public JAXBElement<FindTaskAllSorted> createFindTaskAllSorted(FindTaskAllSorted value) {
        return new JAXBElement<FindTaskAllSorted>(_FindTaskAllSorted_QNAME, FindTaskAllSorted.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskAllSortedResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindTaskAllSortedResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "findTaskAllSortedResponse")
    public JAXBElement<FindTaskAllSortedResponse> createFindTaskAllSortedResponse(FindTaskAllSortedResponse value) {
        return new JAXBElement<FindTaskAllSortedResponse>(_FindTaskAllSortedResponse_QNAME, FindTaskAllSortedResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskById }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindTaskById }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "findTaskById")
    public JAXBElement<FindTaskById> createFindTaskById(FindTaskById value) {
        return new JAXBElement<FindTaskById>(_FindTaskById_QNAME, FindTaskById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskByIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindTaskByIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "findTaskByIdResponse")
    public JAXBElement<FindTaskByIdResponse> createFindTaskByIdResponse(FindTaskByIdResponse value) {
        return new JAXBElement<FindTaskByIdResponse>(_FindTaskByIdResponse_QNAME, FindTaskByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskByIndex }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindTaskByIndex }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "findTaskByIndex")
    public JAXBElement<FindTaskByIndex> createFindTaskByIndex(FindTaskByIndex value) {
        return new JAXBElement<FindTaskByIndex>(_FindTaskByIndex_QNAME, FindTaskByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskByIndexResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindTaskByIndexResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "findTaskByIndexResponse")
    public JAXBElement<FindTaskByIndexResponse> createFindTaskByIndexResponse(FindTaskByIndexResponse value) {
        return new JAXBElement<FindTaskByIndexResponse>(_FindTaskByIndexResponse_QNAME, FindTaskByIndexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskByName }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindTaskByName }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "findTaskByName")
    public JAXBElement<FindTaskByName> createFindTaskByName(FindTaskByName value) {
        return new JAXBElement<FindTaskByName>(_FindTaskByName_QNAME, FindTaskByName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskByNameResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindTaskByNameResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "findTaskByNameResponse")
    public JAXBElement<FindTaskByNameResponse> createFindTaskByNameResponse(FindTaskByNameResponse value) {
        return new JAXBElement<FindTaskByNameResponse>(_FindTaskByNameResponse_QNAME, FindTaskByNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTasksByProjectId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindTasksByProjectId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "findTasksByProjectId")
    public JAXBElement<FindTasksByProjectId> createFindTasksByProjectId(FindTasksByProjectId value) {
        return new JAXBElement<FindTasksByProjectId>(_FindTasksByProjectId_QNAME, FindTasksByProjectId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTasksByProjectIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindTasksByProjectIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "findTasksByProjectIdResponse")
    public JAXBElement<FindTasksByProjectIdResponse> createFindTasksByProjectIdResponse(FindTasksByProjectIdResponse value) {
        return new JAXBElement<FindTasksByProjectIdResponse>(_FindTasksByProjectIdResponse_QNAME, FindTasksByProjectIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveTaskById }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveTaskById }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "removeTaskById")
    public JAXBElement<RemoveTaskById> createRemoveTaskById(RemoveTaskById value) {
        return new JAXBElement<RemoveTaskById>(_RemoveTaskById_QNAME, RemoveTaskById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveTaskByIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveTaskByIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "removeTaskByIdResponse")
    public JAXBElement<RemoveTaskByIdResponse> createRemoveTaskByIdResponse(RemoveTaskByIdResponse value) {
        return new JAXBElement<RemoveTaskByIdResponse>(_RemoveTaskByIdResponse_QNAME, RemoveTaskByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveTaskByIndex }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveTaskByIndex }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "removeTaskByIndex")
    public JAXBElement<RemoveTaskByIndex> createRemoveTaskByIndex(RemoveTaskByIndex value) {
        return new JAXBElement<RemoveTaskByIndex>(_RemoveTaskByIndex_QNAME, RemoveTaskByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveTaskByIndexResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveTaskByIndexResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "removeTaskByIndexResponse")
    public JAXBElement<RemoveTaskByIndexResponse> createRemoveTaskByIndexResponse(RemoveTaskByIndexResponse value) {
        return new JAXBElement<RemoveTaskByIndexResponse>(_RemoveTaskByIndexResponse_QNAME, RemoveTaskByIndexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveTaskByName }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveTaskByName }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "removeTaskByName")
    public JAXBElement<RemoveTaskByName> createRemoveTaskByName(RemoveTaskByName value) {
        return new JAXBElement<RemoveTaskByName>(_RemoveTaskByName_QNAME, RemoveTaskByName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveTaskByNameResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveTaskByNameResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "removeTaskByNameResponse")
    public JAXBElement<RemoveTaskByNameResponse> createRemoveTaskByNameResponse(RemoveTaskByNameResponse value) {
        return new JAXBElement<RemoveTaskByNameResponse>(_RemoveTaskByNameResponse_QNAME, RemoveTaskByNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnbindTaskByProjectId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UnbindTaskByProjectId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "unbindTaskByProjectId")
    public JAXBElement<UnbindTaskByProjectId> createUnbindTaskByProjectId(UnbindTaskByProjectId value) {
        return new JAXBElement<UnbindTaskByProjectId>(_UnbindTaskByProjectId_QNAME, UnbindTaskByProjectId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnbindTaskByProjectIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UnbindTaskByProjectIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "unbindTaskByProjectIdResponse")
    public JAXBElement<UnbindTaskByProjectIdResponse> createUnbindTaskByProjectIdResponse(UnbindTaskByProjectIdResponse value) {
        return new JAXBElement<UnbindTaskByProjectIdResponse>(_UnbindTaskByProjectIdResponse_QNAME, UnbindTaskByProjectIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateTaskById }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateTaskById }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "updateTaskById")
    public JAXBElement<UpdateTaskById> createUpdateTaskById(UpdateTaskById value) {
        return new JAXBElement<UpdateTaskById>(_UpdateTaskById_QNAME, UpdateTaskById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateTaskByIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateTaskByIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "updateTaskByIdResponse")
    public JAXBElement<UpdateTaskByIdResponse> createUpdateTaskByIdResponse(UpdateTaskByIdResponse value) {
        return new JAXBElement<UpdateTaskByIdResponse>(_UpdateTaskByIdResponse_QNAME, UpdateTaskByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateTaskByIndex }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateTaskByIndex }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "updateTaskByIndex")
    public JAXBElement<UpdateTaskByIndex> createUpdateTaskByIndex(UpdateTaskByIndex value) {
        return new JAXBElement<UpdateTaskByIndex>(_UpdateTaskByIndex_QNAME, UpdateTaskByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateTaskByIndexResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateTaskByIndexResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.smochalkin.ru/", name = "updateTaskByIndexResponse")
    public JAXBElement<UpdateTaskByIndexResponse> createUpdateTaskByIndexResponse(UpdateTaskByIndexResponse value) {
        return new JAXBElement<UpdateTaskByIndexResponse>(_UpdateTaskByIndexResponse_QNAME, UpdateTaskByIndexResponse.class, null, value);
    }

}
